#!/usr/bin/env python3

# Salsa Group Auditer
# Copyright 2018 James Cowgill
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
from debian import deb822
import json
import os
import requests
import sys
import urllib.parse

API_URL="https://salsa.debian.org/api/v4/"

class SalsaSession:
    '''Wrapper around requests.Session with some helper methods'''
    def __init__(self):
        self._session = requests.Session()
        with open("TOKEN", 'r') as f:
            self._token = f.read().strip()

    def request(self, method, *args, **kwargs):
        '''
        Runs a request
         args = parts of request, url encoded, then joined with /
         kwargs = passed to requests api
        '''

        # Construct request URL
        url = API_URL + '/'.join((urllib.parse.quote_plus(str(x)) for x in args))

        # Add our private token header
        headers = kwargs.pop("headers", {})
        headers["Private-Token"] = self._token

        # Log all non-get queries
        if method != "get":
            print(method, url, kwargs)

        # Perform the request and check for errors
        r = self._session.request(method, url, headers=headers, **kwargs)
        r.raise_for_status()
        if r.status_code != 204:
            return r.json()

    def get(self, *args, **kwargs):
        return self.request('get', *args, **kwargs)

    def get_unpaginated(self, *args, **kwargs):
        '''
        Like get, but get the entire unpaginated response and returns a
        generator
        '''
        params = kwargs.pop("params", {})
        params["per_page"] = 100
        params["page"] = 1

        result = []
        while True:
            result_page = self.get(*args, params=params, **kwargs)
            if result_page:
                # If we've got a list, return each element
                # individually. Otherwise we have an object so return
                # the whole result.
                if isinstance(result_page, list):
                    yield from result_page
                    if len(result_page) == 100:
                        params["page"] += 1
                        continue
                else:
                    yield result_page

            break

    def post(self, *args, **kwargs):
        return self.request('post', *args, **kwargs)

    def put(self, *args, **kwargs):
        return self.request('put', *args, **kwargs)

    def delete(self, *args, **kwargs):
        return self.request('delete', *args, **kwargs)

class GroupConfig:
    '''Stores the configuration settings for a group'''
    def __init__(self, path, maintainer, project_options, hooks_present, hooks_absent=[], extra_condition=lambda _: True):
        self.path = path
        self.maintainer = maintainer
        self.project_options = project_options
        self.hooks_present = hooks_present
        self.hooks_absent = hooks_absent
        self.extra_condition = extra_condition

class HookResult:
    '''Hook exists and has correct config'''
    GOOD = 0

    '''Hook exists but has bad config'''
    BAD = 1

    '''Hook does not exist / inactive'''
    NON_EXISTANT = 2

    '''Hook needs source package but none was given'''
    NEEDS_SOURCE = 3

class HookService:
    '''Base class for "service" style hooks'''
    def properties_for_project(self, project, source):
        '''
        Returns dict containing correct properties config for a project
        or NEEDS_SOURCE if source is needed and not given.
        '''
        raise NotImplementedError

    def audit(self, s, project, source):
        live_data = s.get("projects", project["id"], "services", self.name)

        if live_data["active"]:
            good_props = self.properties_for_project(project, source)
            if good_props == HookResult.NEEDS_SOURCE:
                return (HookResult.NEEDS_SOURCE, None)

            # Validate each config entry
            record = []
            if not live_data["push_events"]:
                record.append("push events disabled")
            if not live_data["tag_push_events"]:
                record.append("tag events disabled")

            for key, value in good_props.items():
                if live_data["properties"][key] != value:
                    record.append(key + " is wrong")

            if record:
                return (HookResult.BAD, record)
            else:
                return (HookResult.GOOD, None)
        else:
            return (HookResult.NON_EXISTANT, None)

    def add(self, s, project, source):
        '''
        Adds a service, knowing it does not currently exist
         Returns GOOD or NEEDS_SOURCE
        '''
        props = self.properties_for_project(project, source)
        if props == HookResult.NEEDS_SOURCE:
            return HookResult.NEEDS_SOURCE

        s.put("projects", project["id"], "services", self.name, data=props)
        return HookResult.GOOD

    def delete(self, s, project, source):
        '''Deletes a service'''
        s.delete("projects", project["id"], "services", self.name)
        return HookResult.GOOD

class HookEmail(HookService):
    '''Emails on push service'''
    def __init__(self):
        self.name = "emails-on-push"

    def properties_for_project(self, project, source):
        if source:
            return { "recipients": "dispatch+" + source + "_vcs@tracker.debian.org" }
        else:
            return HookResult.NEEDS_SOURCE

class HookIrker(HookService):
    '''Irker IRC notifications service'''
    def __init__(self, server, irc_uri, channel):
        self.name = "irker"
        self.server = server
        self.irc_uri = irc_uri
        self.channel = channel

    def properties_for_project(self, project, source):
        return  {
            "colorize_messages": True,
            "default_irc_uri": self.irc_uri,
            "recipients": self.channel,
            "server_host": self.server
        }

class HookWebhook:
    '''Base class for "webhook" style hooks'''
    def url_for_project(self, project, source):
        '''
        Returns the URL for this webhook or NEEDS_SOURCE if source is needed
        and not given.
        '''
        raise NotImplementedError

    def url_matches_hook(self, url):
        '''
        Tests if the given URL is "managed" by this webhook.

        The auditor will remove all hooks which this function returns true
        for except for the good hook returned by url_for_project.
        '''
        return False

    def find_hooks(self, s, project, url):
        '''
        Returns a generator which iterates over the hooks managed by this
        class within a project.
        '''

        # Cache the list of hooks inside project
        #  This is a bit of a hack (project should really not be modified here)
        if "hooks" not in project:
            project["hooks"] = s.get("projects", project["id"], "hooks")

        for hook in project["hooks"]:
            if self.url_matches_hook(hook["url"]) or hook["url"] == url:
                yield hook

    def audit(self, s, project, source):
        url = self.url_for_project(project, source)
        if url == HookResult.NEEDS_SOURCE:
            return (HookResult.NEEDS_SOURCE, None)

        hook_count = 0
        record = []
        for hook in self.find_hooks(s, project, url):
            hook_count += 1

            # Test that the hook url is exactly correct
            if hook["url"] != url:
                record.append("incorrect url")
            else:
                if not hook["enable_ssl_verification"]:
                    record.append("ssl verification disabled")

                # Verify list of events matches exactly
                events = set([event for event, enabled in hook.items() if
                              event.endswith("_events") and enabled])
                events_missing = self.events - events
                events_new = events - self.events

                if events_missing:
                    record.append("events disabled: " + ", ".join(events_missing))
                if events_new:
                    record.append("events enabled: " + ", ".join(events_new))

        if hook_count > 1:
            record.append("too many hooks")

        if hook_count > 0:
            if record:
                return (HookResult.BAD, record)
            else:
                return (HookResult.GOOD, None)
        else:
            return (HookResult.NON_EXISTANT, None)

    def add(self, s, project, source):
        '''
        Adds a webhook, knowing it does not currently exist
         Returns GOOD or NEEDS_SOURCE
        '''
        url = self.url_for_project(project, source)
        if url == HookResult.NEEDS_SOURCE:
            return HookResult.NEEDS_SOURCE

        data = {event: True for event in self.events}
        data.update({
            "url": url,
            "enable_ssl_verification": True
        })

        s.post("projects", project["id"], "hooks", data=data)
        return HookResult.GOOD

    def delete(self, s, project, source):
        '''Deletes a webhook'''
        url = self.url_for_project(project, source)
        if url == HookResult.NEEDS_SOURCE:
            return HookResult.NEEDS_SOURCE

        for hook in self.find_hooks(s, project, url):
            s.delete("projects", project["id"], "hooks", hook["id"])
        return HookResult.GOOD

class HookTagPending(HookWebhook):
    '''Hook which marks closed bugs as pending'''
    def __init__(self):
        self.name = "bts pending hook"
        self.events = {'push_events'}

    def url_for_project(self, project, source):
        if source:
            return "https://webhook.salsa.debian.org/tagpending/" + source
        else:
            return HookResult.NEEDS_SOURCE

    def url_matches_hook(self, url):
        return url.startswith("https://webhook.salsa.debian.org/tagpending/")

class HookKGB(HookWebhook):
    '''KGB IRC notifications webhook'''
    def __init__(self, channel):
        self.name = "KGB IRC hook"
        # This is currently all events except for:
        #  confidential_issues_events and job_events
        self.events = {'push_events', 'issues_events', 'merge_requests_events',
                       'tag_push_events', 'note_events', 'pipeline_events',
                       'wiki_page_events'}
        self.channel = channel

    def url_for_project(self, project, source):
        return "http://kgb.debian.net:9418/webhook/?channel=" + self.channel

class AuditResult:
    '''The result of running audit_project'''
    def __init__(self, group, project):
        '''Initializes the result with the arguments of audit_project'''
        self.group = group
        self.project = project
        self.record = []
        self._ignored = False

    def add_record(self, msg, fix=None, submsgs=[]):
        '''
        Marks the audit as failed and adds the given audit record

        fix is either None if the issue cannot be automatically fixed, or a
        callable object accepting no arguments which fixes this issue.

        submsgs is list of detailed failures which will also be fixed by the
        given fix object.
        '''
        assert not self._ignored
        self.record.append((msg, fix, submsgs))

    @property
    def status(self):
        '''Returns the audit status (True for success, False for some errors)'''
        return not self.record

    @property
    def ignored(self):
        '''True if this project has been ignored'''
        return self._ignored

    @ignored.setter
    def ignored(self, value):
        if value:
            assert self.status
        self._ignored = value

    def contains_fixable_records(self):
        '''Returns true if any record entry can be fixed'''
        return any(fix is not None for _, fix, _ in self.record)

def get_projects(s, path, recursive=True):
    '''Returns an iterator of projects (with their properties) in a group'''

    # Process direct descendants first
    yield from s.get_unpaginated("groups", path, "projects",
            params={"order_by": "path", "sort": "asc"})

    # Recurse into subgroups
    if recursive:
        for subgroup in s.get_unpaginated("groups", path, "subgroups",
                params={"order_by": "path", "sort": "asc"}):
            yield from get_projects(s, subgroup['full_path'])

def audit_project(s, group, project):
    '''
    Audits a single project

    project is a dict containing all the data returned by gitlab
    '''

    result = AuditResult(group, project)
    deb_control_source = None

    # Ignore project if the extra condition fails, or projects shared
    # with us
    if not project["path_with_namespace"].startswith(group.path + '/') \
       or not group.extra_condition(project):
        result.ignored = True
        return result

    # Basic project options
    for key, value in group.project_options.items():
        if project[key] != value:
            result.add_record(key + " is not " + repr(value),
                lambda: s.put("projects", project["id"], data={key: value}))

    # debian/control
    control_file = None
    try:
        control_file = s.get(
                "projects",
                project["id"],
                "repository",
                "files",
                "debian/control",
                params={"ref": project["default_branch"]})
    except requests.exceptions.HTTPError:
        pass

    if control_file:
        control_data = None
        try:
            control_data = base64.b64decode(control_file["content"]).decode("utf-8")
        except UnicodeError:
            result.add_record("debian/control: invalid utf-8")

        if control_data:
            control_dict = deb822.Deb822(control_data)
            if "Source" in control_dict:
                deb_control_source = control_dict["Source"]
                if deb_control_source != project["path"]:
                    result.add_record("debian/control: Source does not match project name")
                if control_dict.get("Vcs-Git", "") != project["http_url_to_repo"]:
                    result.add_record("debian/control: wrong Vcs-Git")
                if control_dict.get("Vcs-Browser", "") != project["web_url"]:
                    result.add_record("debian/control: wrong Vcs-Browser")
                if control_dict.get("Maintainer", "") != group.maintainer:
                    result.add_record("debian/control: wrong Maintainer")
            else:
                result.add_record("debian/control: no Source field")
    else:
        result.add_record("debian/control: does not exist")

    # Present hooks
    for hook in group.hooks_present:
        (hook_result, record_extra) = hook.audit(s, project, deb_control_source)
        if hook_result in (HookResult.BAD, HookResult.NON_EXISTANT):
            # We need to ensure hook and hook_result are captured _by
            # value_ and not by reference. This is done by using two
            # dummy default arguments.
            def hook_fixer(hook=hook, hook_result=hook_result):
                if hook_result == HookResult.BAD:
                    hook.delete(s, project, deb_control_source)
                hook.add(s, project, deb_control_source)

            if hook_result == HookResult.NON_EXISTANT:
                record_extra = ["disabled"]

            result.add_record(hook.name, hook_fixer, record_extra)

    # Absent hooks
    for hook in group.hooks_absent:
        (hook_result, _) = hook.audit(s, project, deb_control_source)
        if hook_result in (HookResult.GOOD, HookResult.BAD):
            result.add_record(hook.name,
                    lambda: hook.delete(s, project, deb_control_source),
                    ["enabled, but should not be"])

    return result

def task_audit(s, group, fix=False):
    def print_colour(colour, text):
        if os.isatty(1):
            print(colour, end='')
            print(text, end='')
            print('\033[0m')
        else:
            print(text)

    def print_project_line(project, colour, text):
        print(project['path_with_namespace'] + ': ', end='')
        print_colour(colour, text)

    # Audit all the projects to find out what needs fixing
    to_fix = []
    for project in get_projects(s, group.path):
        result = audit_project(s, group, project)
        if result.ignored:
            print_project_line(project, '\033[93m', "IGNORE")
        elif result.status:
            print_project_line(project, '', "ok")
        else:
            print_project_line(project, '\033[91m', "FAIL")
            for msg, fix_func, submsgs in result.record:
                if fix_func:
                    to_fix.append((project, msg, fix_func))

                print(" ", msg)
                for submsg in submsgs:
                    print("  ", submsg)

    if to_fix:
        # Print all the issues that will be fixes
        print()
        print("Will fix")
        print("========")
        for project, msg, _ in to_fix:
            print_project_line(project, '', msg)

        # Fix them
        print()
        if not fix:
            try:
                fix = input("Continue? [y/n] ").lower() == "y"
            except EOFError:
                fix = False

        if fix:
            for project, msg, fix_func in to_fix:
                fix_func()
                print_project_line(project, '', msg + " (fixed)")

def task_get(s, parts):
    json.dump(list(s.get_unpaginated(*parts)), sys.stdout, indent=2)

def task_import(s, group, project, import_url):
    # Create project
    data = {
            "path": project,
            "namespace_id": s.get("namespaces", group.path)["id"],
            "description": project + " packaging",
            "import_url": import_url
        }
    data.update(group.project_options)
    project_data = s.post("projects", data=data)

    # Setup present hooks
    for hook in group.hooks_present:
        hook.add(s, project_data, project)


GAMES_TEAM = GroupConfig(
        "games-team",
        "Debian Games Team <pkg-games-devel@lists.alioth.debian.org>",
        {"visibility": "public",
         "issues_enabled": False,
         "printing_merge_request_link_enabled": False},
        [HookTagPending(),
         HookEmail(),
         HookKGB("debian-games")],
        [],
        lambda project: not project["path_with_namespace"].startswith(
            ("games-team/attic/", "games-team/unfinished/")),
    )

MULTIMEDIA_TEAM = GroupConfig(
        "multimedia-team",
        "Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>",
        {"visibility": "public",
         "issues_enabled": False,
         "printing_merge_request_link_enabled": False},
        [HookTagPending(),
         HookEmail(),
         HookKGB("debian-multimedia")],
    )

GROUPS = {
    "games":      GAMES_TEAM,
    "multimedia": MULTIMEDIA_TEAM
}

if __name__ == '__main__':
    def get_group(name):
        if name in GROUPS:
            return GROUPS[name]
        else:
            print("unknown group", name)
            sys.exit(1)

    s = SalsaSession()
    if len(sys.argv) > 2 and sys.argv[1] == "audit":
        task_audit(s, get_group(sys.argv[2]), len(sys.argv) > 3 and sys.argv[3] == "-f")
    elif len(sys.argv) > 2 and sys.argv[1] == "get":
        task_get(s, sys.argv[2:])
    elif len(sys.argv) > 4 and sys.argv[1] == "import":
        task_import(s, get_group(sys.argv[2]), sys.argv[3], sys.argv[4])
    else:
        print("usage:")
        print("  audit <group> [-f]             = team repositories audit")
        print("  get <parts...>                 = get arbitrary salsa url")
        print("  import <group> <package> <url> = import package from url")
        sys.exit(1)
