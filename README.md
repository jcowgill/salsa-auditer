# Salsa Group Auditer
This project contains a script which "audits" entire salsa groups to ensure all
projects have the same settings (including hooks).

Currently all group settings are hardcoded into the audit script, but these
should not be too hard to edit.

## Dependencies
- python3
- python3-debian
- python3-requests

## Access Token
First, you must obtain a salsa access token with the "api" scope enabled. This
can be done from this page:
https://salsa.debian.org/profile/personal_access_tokens

Write the token into a file called `TOKEN` in the directory with the
`salsa-auditer.py` script.

## Usage
To audit a group, run this where `GROUP` is the name of the group to audit.

    ./salsa-auditer.py audit GROUP

To audit a group and adjust any settings automatically, run:

    ./salsa-auditer.py audit GROUP -f

To import a project into a group with the correct settings run:

    ./salsa-auditer.py import GROUP PROJECT IMPORT_URL
